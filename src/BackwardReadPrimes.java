import java.util.ArrayList;

public class BackwardReadPrimes {


    public static String backwardsPrime(long start, long end) {

        ArrayList<Long> table = new ArrayList<>();
        String result = "";
        boolean flag;
        long i = start;
        while (i <= end) {

            flag = false;
            if (i <= 2 || i / 10 == 0 || i % 2 == 0) {
                i++;
                continue;
            }
            flag = isSecondFlag(flag, i);

            if (!flag) {
                table.add(i);
            }
            i++;
        }
        System.out.println(table);


        for (long number : table) {

            long revers = 0;
            boolean secondFlag = false;

            while (number != 0) {
                long digit;
                digit = number % 10;
                revers = revers * 10 + digit;
                number = number / 10;

            }
            if (revers != number) {
                if (revers % 2 == 0) {
                    continue;
                }
                secondFlag = isSecondFlag(secondFlag, revers);
                if (!secondFlag) {

                    if (result == "") {
                        System.out.println(result + " result: ");
                        System.out.println(String.valueOf(number) + " table: ");
                        result = String.valueOf(number);
                    } else {
                        result += " ";
                        result += String.valueOf(number);
                    }
                }
            }
        }
        System.out.println(result);
        return result;
    }

    private static boolean isSecondFlag(boolean secondFlag, long revers) {
        for (long q = (long) Math.sqrt(revers); q > 2; q--) {
            if (revers % q == 0 || secondFlag) {
                secondFlag = true;
                break;
            }
        }
        return secondFlag;
    }
}


